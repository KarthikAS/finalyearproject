package com.iprob.photo;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;



import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class FirestationActivity extends Activity {
	
	static ArrayList<String> arrayl;
	static ArrayAdapter<String> aa;
	LocationManager locationManager;
	 ListView lv;
	 MyLocationListener locationListener;
	 
    /** Called when the activity is first created. */  
	 
	       
    @Override   
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.firelist);
           
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        arrayl = new ArrayList<String>();
        
        Button btn = (Button) findViewById(R.id.btn);
        lv = (ListView) findViewById(R.id.lv);
        
        aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        
        btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) { 
				// TODO Auto-generated method stub  
				
				Criteria ct = new Criteria();
				   locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				String best = locationManager.getBestProvider(ct, true);
				
				locationListener=new MyLocationListener();
				locationManager.requestLocationUpdates(
                		best, 
                 		0, 
                 		0,
                 		locationListener);
                Toast.makeText(FirestationActivity.this, "best "+best, 6000).show();
				
			}   
		});    
           
    }
    String Name,address;   
  
    
    public void post(String s,String s2) {
    	String line="";  
		try {   
			HttpPost httpPost = new HttpPost(
					"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+s+","+s2+"&radius=5000&name=police&sensor=false&key=AIzaSyAJG6A7z0Z3pLWurrfoBe739ho4f1IP9YA");
			HttpParams httpParameters = new BasicHttpParams();
			System.out.println(
					"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+s+","+s2+"&radius=5000&name=police&sensor=false&key=AIzaSyAJG6A7z0Z3pLWurrfoBe739ho4f1IP9YA");
			int timeoutConnection = 2000;  
			HttpConnectionParams.setConnectionTimeout(httpParameters,      
				timeoutConnection);
			int timeoutSocket = 2000;       
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			line = EntityUtils.toString(httpEntity);
			   
			
			try {
				JSONObject jo1 = new JSONObject(line);
				JSONArray ja = jo1.getJSONArray("results");
	//			JSONArray ja1 = new JSONArray(line); 
		//		JSONObject jo = ja1.getJSONObject(0); 
			//	JSONArray ja = jo.getJSONArray("results"); 
				
				for(int i=0;i<ja.length();i++){
					
					String Name = ja.getJSONObject(i).get("name").toString();
					String address = ja.getJSONObject(i).get("vicinity").toString();
					
					aa.add(Name+"\n"+address);
					    
				}
			} catch (Exception e) {
				System.out.println("xxxxxxxx1" + line);

			}
			lv.setAdapter(aa);
			System.out.println("responce" + line);
			Log.v("log", "  responce= " + line);

		} catch (Exception e) {
			System.out.println("xxxxxxxx1" + line);
			System.out.println("the exception is "+e.getMessage());
           

		}

	}
    
	private class MyLocationListener implements LocationListener {

		public void onLocationChanged(Location location) {
			String message = String.format(
					"New Location of your friend \n Longitude: %1$s \n Latitude: %2$s",
					location.getLongitude(), location.getLatitude()
			);
			Toast.makeText(FirestationActivity.this, "got loc fix", 6000).show();
			
			post(""+location.getLatitude(),""+location.getLongitude());
			locationManager.removeUpdates(locationListener);
			locationManager = null;
			locationListener = null;
			  
			
     	   //  smanager.sendTextMessage(incomingno, null, "The locations is "+message, null, null);
			
  
			
         
		}

		public void onStatusChanged(String s, int i, Bundle b) {
		
			
					
		}

		public void onProviderDisabled(String s) {
			
			
		}

		public void onProviderEnable(String s) {
			
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

	}
}