package com.iprob.photo;

import java.io.IOException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.SmsMessage;
import android.widget.Toast;

public class smsReceiver extends BroadcastReceiver {

	String incomingno;
	static LocationManager locationManager;
	static MyLocationListener locationListener;
	Context cntxt;
	static LocationManager locationMgr;
	SharedPreferences sp;
	SmsManager smanager;         
	static MyTrackListener MTListener;   
	private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
	private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
	private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
	private MediaRecorder recorder = null;        
	private int currentFormat = 0;
	private int output_formats[] = { MediaRecorder.OutputFormat.MPEG_4,
			MediaRecorder.OutputFormat.THREE_GPP };
	private String file_exts[] = { AUDIO_RECORDER_FILE_EXT_MP4,
			AUDIO_RECORDER_FILE_EXT_3GP };
    
	public static String BalanceWallet = "200";
	
	@Override     
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub   
		cntxt=context;
		sp = PreferenceManager.getDefaultSharedPreferences(context);
	    smanager=SmsManager.getDefault();
	      
		Bundle bundle = intent.getExtras();        
        SmsMessage[] msgs = null;
        String str = "";                 
              
		if (bundle != null)     
	    {        
	            //---retrieve the SMS message received---
	            Object[] pdus = (Object[]) bundle.get("pdus");
	            msgs = new SmsMessage[pdus.length];            
	            for (int i=0; i<msgs.length; i++){   
	            	msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);                
	                incomingno = msgs[i].getOriginatingAddress();                     
	                String body= msgs[i].getMessageBody().toString();
	                   
	                Toast.makeText(cntxt, "sms received "+body, Toast.LENGTH_LONG).show();           
	                TelephonyManager tm = (TelephonyManager) cntxt.getSystemService(Context.TELEPHONY_SERVICE);
	                   
	                String imie=tm.getDeviceId();
	                               
//	                if(!imie.contains("911338900908635"))
//	                {              
//	                	Toast.makeText(cntxt, "Invalid imei", 600000).show();
//	                	return;
//	                }                            
	                     
	                if(body.contains("recharge")){   
	                	String[] temp = body.split("\\$");  
	                	BalanceWallet = temp[1];    
	                	  
	                }                                
	                                                     
	                if(body.equalsIgnoreCase("loc")){
	                        
	                	final Criteria criteria = new Criteria();
	                	locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	            		final String bestProvider = locationManager.getBestProvider(criteria, true);
	            		Toast.makeText(context, ""+bestProvider, 600000).show();
	                    locationListener=new MyLocationListener();
	                    locationManager.requestLocationUpdates(
	                     		bestProvider,   
	                     		0,    
	                     		0,   
	                     		locationListener);
	                	                   
	                 	                                
	                }            
	                              
	                if(body.equalsIgnoreCase("photo")){   
	                	             
	                	Intent in = new Intent(context,OneShotPreviewActivity.class);
	                	in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        			context.startActivity(in);
	                }    
	                if(body.equalsIgnoreCase("track")){  
	                	   
	                	     
	                	final Criteria criteria = new Criteria();
	                	                	
	                	locationMgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	            		final String bestProvider = locationMgr.getBestProvider(criteria, true);
	            		     
	            		Toast.makeText(cntxt, ""+bestProvider, 600000).show(); 
	            		
	    				MTListener = new MyTrackListener(); 
	    				locationMgr.requestLocationUpdates(
	                     		bestProvider, 
	                     		10000,   
	                     		0,
	                     		MTListener);   
	                	                                
	                }           
	                   
	                if(body.contains("stop")){      
	                	   
	                	if (locationMgr != null && MTListener != null){
	                		Toast.makeText(context, "gps stopped", 6000000).show();
	                		locationMgr.removeUpdates(MTListener);
	                		locationMgr=null;       
	                		MTListener=null;
		                 }    
	                	   
	                	             
	                }else if(body.contains("audioon")){
	                	
	                	//startRecording();      
	                	
	                }else if(body.contains("audioof")){
	                	
//	                	stopRecording();
//	                	
//	                	try {   
//	    	                GMailSender sender = new GMailSender("dayadad@gmail.com", "achievement");
//	    	                sender.sendMail("Iprob",   
//	    	                        "this is iprobAndroid app",   
//	    	                        "dayadad@gmail.com",   
//	    	                        "dayanand.shine@gmail.com","/sdcard/daya.mp4");   
//	    	            } catch (Exception e) {    
//	    	            	   
//	    	                Log.e("SendMail", e.getMessage(), e);   
//	    	                
//	    	            }      
	                  	            
	                }else if(body.equalsIgnoreCase("close")){     
	                	   
	                	OneShotPreviewActivity.os.finish();
	                }
	                   
	                if(body.contains("AWAKE")){  
	                       	      
	                		MediaPlayer mp = new MediaPlayer();    
	        				try {            
	        					      
	        					mp.setDataSource("/sdcard/sleep.amr");   
	        					mp.prepare();    
	        					mp.start();                                   
	        					         
	        				} catch (IllegalArgumentException e) {
	        					// TODO Auto-generated catch block
	        					e.printStackTrace();
	        					   
	        				} catch (IllegalStateException e) {
	        					// TODO Auto-generated catch block
	        					e.printStackTrace();
	        				} catch (IOException e) {
	        					// TODO Auto-generated catch block
	        					e.printStackTrace();
	        				}
	                	                    
	                }   
	        
	            }
	    }
	}	 
	
	public void stopListening(LocationManager locationManager) {

		try {
			if (locationManager != null && locationListener != null) {
				locationManager.removeUpdates(locationListener);
				locationManager = null;
				locationListener = null;
			}

		} catch (final Exception ex) {   

		}  
	}

	private class MyLocationListener implements LocationListener {

		public void onLocationChanged(Location location) {
			String message = String
					.format("12345 New Location of your friend \n Longitude: %1$s \n Latitude: %2$s",
							location.getLongitude(), location.getLatitude());
			Toast.makeText(cntxt, "got loc", 6000).show();
			smanager.sendTextMessage(
					incomingno,  
					null,
					"loc1234 " + location.getLatitude() + ","
							+ location.getLongitude(), null, null);
			stopListening(locationManager);

			// String FirstNo = sp.getString("First No",null);
			// String SecondNo = sp.getString("Second No",null);
			// String PoliceNo = sp.getString("Police No",null);
			//
			// smanager.sendTextMessage(FirstNo, null,
			// "The locations is "+message, null, null);
			// smanager.sendTextMessage(SecondNo, null,
			// "The locations is "+message, null, null);
			// smanager.sendTextMessage(PoliceNo, null,
			// "The locations is "+message, null, null);
			//

		}

		public void onStatusChanged(String s, int i, Bundle b) {

		}
		
		public void onProviderDisabled(String s) {

		}

		public void onProviderEnabled(String s) {

		}

	}

	private class MyTrackListener implements LocationListener {

		public void onLocationChanged(Location location) {
			String message = String
					.format("New Location of your friend \n Longitude: %1$s \n Latitude: %2$s",
							location.getLongitude(), location.getLatitude());
			// stopListening(locationManager);
			smanager.sendTextMessage(
					incomingno,
					null,
					"loc1234 " + location.getLatitude() + ","
							+ location.getLongitude(), null, null);
			     
		}      
		   	
		public void onStatusChanged(String s, int i, Bundle b) {
			
		}
		
		public void onProviderDisabled(String s) {
			
		}
		
		public void onProviderEnabled(String s) {

		}

	}

	private void startRecording() {

		Toast.makeText(cntxt, "audio recording", 6000).show();
		recorder = new MediaRecorder();

		recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		recorder.setOutputFile("/sdcard/daya.mp4");

		recorder.setOnErrorListener(errorListener);
		recorder.setOnInfoListener(infoListener);

		try {
			recorder.prepare();
			recorder.start();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void stopRecording() {
		if (null != recorder) {

			Toast.makeText(cntxt, "audio stopeed", 6000).show();
			recorder.stop();
			recorder.reset();
			recorder.release();

			recorder = null;
		}
	}

	private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
		@Override
		public void onError(MediaRecorder mr, int what, int extra) {
			Toast.makeText(cntxt, "Error: " + what + ", " + extra,
					Toast.LENGTH_SHORT).show();
		}
	};

	private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
		@Override
		public void onInfo(MediaRecorder mr, int what, int extra) {
			Toast.makeText(cntxt, "Warning: " + what + ", " + extra,
					Toast.LENGTH_SHORT).show();
		}
	};

}
