package com.iprob.photo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.Facebook;
import com.facebook.android.SessionStore;
   
   

public class IProbActivity extends Activity  {
    /** Called when the activity is first created. */
	   
	boolean x = false;      
	SharedPreferences sp;    
	static public ShakeListener mShaker;  
	SmsManager smanager;          
	LocationManager locationManager;    
	MyLocationListener locationListener;
	TextView tv,tv1;  
	private Facebook mFacebook;  
	Handler handler;   
	static IProbActivity ib;      
	     
    @Override  
    public void onCreate(Bundle savedInstanceState) {
    	  
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
           
//        Intent intent = new Intent(this,MyService.class);
//        startService(intent);   
           
        ib = this;     
        sp = PreferenceManager.getDefaultSharedPreferences(this);
    	//String data = sp.getString("threshold",null);
        smanager=SmsManager.getDefault();   
        Button button = (Button) findViewById(R.id.button);
        Button nearest = (Button) findViewById(R.id.nearest);
               
//        button.setVisibility(View.GONE);   
//         nearest.setVisibility(View.GONE);  
                                            
        nearest.setOnClickListener(new OnClickListener() {
			              
			@Override     
			public void onClick(View arg0) {   
				// TODO Auto-generated method stub
				Intent intent = new Intent(IProbActivity.this,FirestationActivity.class);
				startActivity(intent);
			}   
		});   
        
        button.setOnClickListener(new OnClickListener() {
        	          
			@Override            
			public void onClick(View arg0) {   
				// TODO Auto-generated method stub  
				Intent intent = new Intent(IProbActivity.this,Setting.class);
				startActivity(intent); 
				          
			}             
		});        
          
        nearest.setText("Nearest station");
                       
        mFacebook 	= new Facebook("452701744757930");
              
		  
		SessionStore.restore(mFacebook, this);

		if (mFacebook.isSessionValid()) {
		//	mFacebookCb.setChecked(true);  
				  
			String name = SessionStore.getName(this);
			name		= (name.equals("")) ? "Unknown" : name;
  
		}     
		
        tv1 = (TextView) findViewById(R.id.tv1);

        
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
                                          
        Enable();
                 
    }                   
       
    public void Enable(){
    	  
    	
   	 final Vibrator vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
	 mShaker = new ShakeListener(this);
	 mShaker.setOnShakeListener(new ShakeListener.OnShakeListener () {
		 
	 public void onShake(float speed)
	 { 
		 mShaker.pause();
		      
		       
		// tv1.setText("Force on the acceleremeter is "+speed);
		 Toast.makeText(getApplicationContext(), "this is onshake", 60000).show();
		        
		 System.out.println("onshake");
	     vibe.vibrate(200);     
	           
	     sendSMS();        
	     //alertDialogtwoButton();     
	         
//	     handler = new Handler();   
//		 handler.postDelayed(runnable, 15000);
	               
//	     Intent intent = new Intent(IProbActivity.this,OneShotPreviewActivity.class);
//	     startActivity(intent);
	          
	            
	   }    
     
	     
	 
	@Override
	public void onCamera(float speed) {
		// TODO Auto-generated method stub  
		//mShaker.pause(); 
		 tv1.setText("Force on the acceleremeter is "+speed);    
		   
	}  
	 });     
    }   
          
       
         
AlertDialog alertDialog;
@SuppressWarnings("deprecation")
public void alertDialogtwoButton(){   
		      
		alertDialog = new AlertDialog.Builder(IProbActivity.this)
			.create();
		alertDialog.setTitle("Warning!");
			alertDialog.setMessage("Really emergency????");
			alertDialog.setButton("YES", new DialogInterface.OnClickListener() {
				    
				@Override   
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
//					handler.removeCallbacks(runnable);
//					 sendSMS();        
					                                       
				}         
			});
		 alertDialog.setButton2("NO", new DialogInterface.OnClickListener() {
         public void onClick(DialogInterface dialog, int id) {
                      
        	 //handler.removeCallbacks(runnable);
        	 
         	      
            }   
        });
			
			alertDialog.show();
			
		       
	}    
                  
    public void sendSMS(){
    	  String FirstNo = sp.getString("First No",null);   
 	     String SecondNo = sp.getString("Second No",null);
  	     String PoliceNo = sp.getString("Police No",null);   
 	                
 	     smanager.sendTextMessage(FirstNo, null, "crack detected, please check", null, null);
 	     smanager.sendTextMessage(SecondNo, null, "crack detected, please check", null, null);   
 	     smanager.sendTextMessage(PoliceNo, null, "crack detected, please check", null, null);   
 	                  
// 	    smanager.sendTextMessage(FirstNo, null, "The Person is in problem. to track his location please send loc", null, null);
//	    smanager.sendTextMessage(SecondNo, null, "The Person is in problem. to track his location please send loc", null, null);   
//	    smanager.sendTextMessage(PoliceNo, null, "The Person is in problem. to track his location please send loc", null, null);   
	               
	    Toast.makeText(getApplicationContext(), "Your message has been sent.", 6000000).show();
    }            
                 
    @Override      
    public void onDestroy(){
    	super.onDestroy();
          
    	   
    }
    @Override
    public void onResume(){
    	super.onResume();
    	
       
    	
    }
    @Override
    public void onPause(){
    	super.onPause();
    	
    } 
      
	private class MyLocationListener implements LocationListener {

		public void onLocationChanged(Location location) {
			   
			tv.setText(location.getLatitude()+","+location.getLongitude());
			float speed = location.getSpeed();
			                 
		   if(locationManager!=null){
			      
			   locationManager.removeUpdates(locationListener);
			   locationManager = null;
			   locationListener = null;
			        
		   }   
	
		}

		public void onStatusChanged(String s, int i, Bundle b) {
		
			
					
		}

		public void onProviderDisabled(String s) {
			
			
		}

		public void onProviderEnabled(String s) {
			
			
		}

	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) { 
    	Toast.makeText(getApplicationContext(), "inside menu", 600000).show();
    	//menu.add(Menu.NONE, 0, 0, "Show settings");
   // 	menu.add(Menu.NONE, 1, 1, "Facebook settings");
    	return super.onCreateOptionsMenu(menu);
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case 0:
    			startActivity(new Intent(this, Setting.class));
    			return true;
    		case 1:
    			startActivity(new Intent(this, TestConnect.class));
    	}
    	return false;
    }
    
	private void postToFacebook(String review) {	
		
		
		AsyncFacebookRunner mAsyncFbRunner = new AsyncFacebookRunner(mFacebook);
		
		Bundle params = new Bundle();   
    		
		    
		params.putString("message", review);
		params.putString("name", "Dexter");
		params.putString("caption", "iprob");
		params.putString("link", "http://www.londatiga.net");
		params.putString("description", "Dexter, seven years old dachshund who loves to catch cats, eat carrot and krupuk");
		params.putString("picture", "http://twitpic.com/show/thumb/6hqd44");
		
		   
		mAsyncFbRunner.request("me/feed", params, "POST", new WallPostListener());
	}
	
	private final class WallPostListener extends BaseRequestListener {
		@Override
        public void onComplete(final String response) {
        	   
//        	mRunOnUi.post(new Runnable() {
//        		@Override
//        		public void run() {
//        		      
//        			
//        			//Toast.makeText(IProbActivity.this, "Posted to Facebook", Toast.LENGTH_SHORT).show();
//        		}
//        	});   
        }
    }
    
}